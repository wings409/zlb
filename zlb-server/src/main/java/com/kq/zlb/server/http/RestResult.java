package com.kq.zlb.server.http;


import org.springframework.http.HttpStatus;

public class RestResult {

    private int code;
    private String msg;
    private Object data;

    public RestResult() {
    }

    public RestResult(ResultCode resultCode) {
        this.code = resultCode.code();
        this.msg = resultCode.msg();
    }

    public RestResult(ResultCode resultCode, Object data) {
        this.code = resultCode.code();
        this.msg = resultCode.msg();
        this.data = data;
    }

    public static RestResult error() {
        return error(HttpStatus.INTERNAL_SERVER_ERROR.value(), "未知异常，请联系管理员");
    }

    public static RestResult BUSINESS_EXCEPTION() {
        return new RestResult(CommonCode.BUSINESS_EXCEPTION);
    }

    public static RestResult WRONG_APPINFO() {
        return new RestResult(CommonCode.WRONG_APPINFO);
    }

    public static RestResult ILLEGAL_TOKEN() {
        return new RestResult(CommonCode.ILLEGAL_TOKEN);
    }

    public static RestResult EXPIRED_TOKEN() {
        return new RestResult(CommonCode.EXPIRED_TOKEN);
    }

    public static RestResult SUCCESS() {
        return new RestResult(CommonCode.SUCCESS);
    }

    public static RestResult SUCCESS(Object data) {
        return new RestResult(CommonCode.SUCCESS,data);
    }

    public static RestResult error(String msg) {
        return error(HttpStatus.INTERNAL_SERVER_ERROR.value(), msg);
    }

    public static RestResult error(int code, String msg) {
        RestResult r = new RestResult();
        r.setCode(code);
        r.setMsg(msg);
        return r;
    }

    public static RestResult ok(String msg) {
        RestResult r = new RestResult();
        r.setCode(HttpStatus.OK.value());
        r.setMsg(msg);
        return r;
    }

    public static RestResult ok(Object data) {
        RestResult r = new RestResult();
        r.setCode(HttpStatus.OK.value());
        r.setMsg(HttpStatus.OK.name());
        r.setData(data);
        return r;
    }

    public static RestResult ok() {
        return new RestResult();
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "RestResult{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }
}
