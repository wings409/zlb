package com.kq.zlb.server.model;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: See
 * @description: PageRequest
 * @create: 2019-08-25 19:26
 */
@Data
public class PageRequest {
    private int pageNum = 1;
    private int pageSize = 10;
    private Map<String, ColumnFilterCV> columnFilters = new HashMap<>();
}
