package com.kq.zlb.server;

import com.kq.zlb.server.utils.IpUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @Description:
 * @Author: qiqinbo
 * @Date: 2021/7/18 18:17
 **/
@Slf4j
@SpringBootApplication
public class ZlbServerApplication {
    public static void main( String[] args ) {
        IpUtils.copyIpSysProperties();
        ConfigurableApplicationContext applicationContext = SpringApplication.run(ZlbServerApplication.class, args);
        log.info("Zlb [{}] Service Start OK", applicationContext
                .getEnvironment().getProperty("spring.profiles.active"));
    }
}
