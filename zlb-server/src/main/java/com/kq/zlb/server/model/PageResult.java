package com.kq.zlb.server.model;

import lombok.Data;

import java.util.List;

/**
 * @author: See
 * @description: PageResult
 * @create: 2019-08-25 19:26
 */
@Data
public class PageResult {
    private int pageNum;
    private int pageSize;
    private long totalSize;
    private int totalPages;
    private List<?> content;
}
