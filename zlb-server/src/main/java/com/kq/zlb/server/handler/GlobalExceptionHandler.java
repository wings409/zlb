package com.kq.zlb.server.handler;

import com.kq.zlb.server.http.ErrorResult;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * @author: See
 * @description: 全局异常拦截器
 * @create: 2020-02-21 11:30
 */

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public ErrorResult jsonErrorHandler(HttpServletRequest req, Exception e) {
        ErrorResult result = new ErrorResult();
        result.setMsg(e.getCause().getMessage());
        result.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
        result.setData("");
        result.setUrl(req.getRequestURL().toString());
        return result;
    }
}
