package com.kq.zlb.server.model;

import lombok.Data;

/**
 * @author: See
 * @description: 分页过滤
 * @create: 2019-08-25 19:26
 */
@Data
public class ColumnFilterCV {
    private String name;
    private String value;
}
