package com.kq.zlb.server.http;

import lombok.ToString;

/**
 * @author 4you
 * @date 2019/7/10
 */
@ToString
public enum CommonCode implements ResultCode {
    SUCCESS(200, "调用成功"),
    BUSINESS_EXCEPTION(500, "业务异常"),
    WRONG_APPINFO(10000, "appKey, appSecret 不正确"),
    ILLEGAL_TOKEN(10001, "token 非法"),
    EXPIRED_TOKEN(10002,"token 过期");

    int code;
    String msg;

    CommonCode(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    @Override
    public int code() {
        return code;
    }

    @Override
    public String msg() {
        return msg;
    }
}
