package com.kq.zlb.server.http;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author: See
 * @description: 全局异常拦截器
 * @create: 2020-02-21 11:30
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ErrorResult {
	private int code;
	private String msg;
	private Object data;
	private String url;
}
